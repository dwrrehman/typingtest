/// typingtest:
///
///     a C program times how fast and accurate you type english words.
///
///     2003227.022116
///
///     written by Daniel Rehman

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>

typedef ssize_t nat;

const nat left_size = 25;
const nat right_size = 25;
const long completed_color = 75L;
const long score_color = 81L;

const char quit_char = '/';

struct termios terminal = {0};

const char
    *set_cursor = "\033[%lu;%luH", // unused, right now.
    *clear_line = "\033[2K";

#define set_color "\033[38;5;%lum"
#define reset_color "\033[0m"

static void restore_terminal() {
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &terminal) < 0) {
        perror("tcsetattr(STDIN_FILENO, TCSAFLUSH, &terminal))");
        abort();
    }
}

static void configure_terminal() {
    if (tcgetattr(STDIN_FILENO, &terminal) < 0) { perror("tcgetattr(STDIN_FILENO, &terminal)"); abort(); }
    atexit(restore_terminal);
    struct termios raw = terminal;
    raw.c_lflag &= ~(ECHO | ICANON);
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) < 0) {
        perror("tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw)");
        abort();
    }
}

static char get_character() {
    char c = 0;
    const ssize_t n = read(STDIN_FILENO, &c, 1);
    if (n < 0) {
        printf("n < 0 : ");
        perror("read(STDIN_FILENO, &c, 1) syscall");
        abort();
    } else if (n == 0) {
        printf("n == 0 : ");
        perror("read(STDIN_FILENO, &c, 1) syscall");
        abort();
    } else return c;
}

// must call free on the return value.
static const char** generate_word_list(const nat size, char** words, const nat word_count) {
    const char** list = calloc(size, sizeof(const char*));
    for (nat i = 0; i < size; i++) list[i] = words[random() % word_count];
    return list;
}

// words in the dictionary are seperated by only newlines.
static nat load_dictionary(const char* dict_filepath, char*** words) {
    FILE* file = fopen(dict_filepath, "r");
    if (!file) { perror("fopen"); exit(1); }
    char line[128] = {0};
    nat count = 0;
    while (fgets(line, 127, file)) {
        line[strlen(line) - 1] = 0;
        *words = realloc(*words, sizeof(char*) * (count + 1));
        (*words)[count++] = strdup(line);
    }
    fclose(file);
    return count;
}

// must give positive test_count.
static const char* concat_all(const char** test, const nat test_count) {
    nat result_length = strlen(test[0]);
    char* result = calloc(result_length + 1, sizeof(char));
    strcpy(result, test[0]);
    for (nat i = 1; i < test_count; i++) {
        result = realloc(result, sizeof(char) * (result_length + 2 + strlen(test[i])));
        strcat(result, " ");
        strcat(result, test[i]);
        result_length += 1 + strlen(test[i]);
    }
    return result;
}

static void display(char *history, nat history_length, const char *string, nat string_index) {
    printf("\r");
    printf("%s", clear_line);
    printf(set_color, completed_color);
    const nat left_written_count = history_length - fdim(history_length, left_size);
    for (nat i = 0; i < left_size - left_written_count; i++) printf(" ");
    for (nat i = fdim(history_length, left_size); i < history_length; i++) fputc(history[i], stdout);
    printf(reset_color);
    nat written_count = 0;
    for (nat i = 0; i < fmin(strlen(string + string_index), right_size); i++) {
        fputc(string[string_index + i], stdout);
        written_count++;
    }
    for (nat i = 0; i < right_size - written_count; i++) printf(" ");
    fflush(stdout);
}

static void push(char c, char** history, nat* history_length) {
    *history = realloc(*history, sizeof(char) * ((*history_length) + 1));
    (*history)[(*history_length)++] = c;
}

int main(const int argc, const char** argv) {
    nat test_count = 100;
    if (argc == 2) test_count = atoi(argv[1]);
    srandom((unsigned)time(NULL));
    configure_terminal();
    char** words = NULL;
    const nat word_count = load_dictionary("/Users/deniylreimn/Documents/projects/typingtest/common.txt", &words);
    const char** test = generate_word_list(test_count, words, word_count);
    const char* string = concat_all(test, test_count);
    nat correct_completed = 0, char_correct = 0, words_completed = 0, string_start = 0, char_incorrect = 0, history_length = 0;
    
    printf("[press space to begin]");
    fflush(stdout);
    char* history = NULL, c = get_character();
    const time_t begin = time(NULL);
        
    while (c != quit_char && words_completed < test_count) {
        
        display(history, history_length, string, string_start);
        c = get_character();
        
        if (c == '\n' || c == '\t') continue;
        
        else if (c == 127) {
            if (char_incorrect) {
                history[--history_length] = 0;
                char_incorrect--;
            } else if (char_correct) {
                string_start--;
                history[--history_length] = 0;
                char_correct--;
            }
        } else {
            
            if (c == ' ' && (char_correct || char_incorrect)) {
                push(c, &history, &history_length);
                if (char_correct != strlen(test[words_completed])) printf("\a");
                else correct_completed++;
                
                char_correct = 0;
                char_incorrect = 0;
                words_completed++;
                
                while (string[string_start] != ' ' && string[string_start]) string_start++;
                if (string[string_start]) string_start++;
                
            } else if (c == test[words_completed][char_correct] && !char_incorrect) {
                push(c, &history, &history_length);
                char_correct++;
                string_start++;
            } else {
                push(c, &history, &history_length);
                char_incorrect++;
            }
        }
    }
    const time_t end = time(NULL);
    
    free(test);
    for (nat i = 0; i < word_count; i++) free(words[i]);
    free(words);
    puts("");
    restore_terminal();
    
    const double seconds_taken = ((double) (end - begin));
    const double minutes_taken = seconds_taken / 60.0;
    printf("score:  %ld / %ld  in %.3f seconds\n", correct_completed, words_completed, seconds_taken);
    printf(set_color "     %.2f WPM    ", score_color, (double) correct_completed / (double) minutes_taken);
    if (words_completed) printf("accuracy: %3.1f%%\n", (double) correct_completed / (double) words_completed * 100.0); else printf("\n");
    printf(reset_color);
}
